# AGEPINFO's Secret Archive

This repository serves two purposes:
- it hosts public keys and references used in AGEPoly's infrastructure
- it holds an encrypted copy of AGEPINFO's Secret Archive

In normal circumstances, only the IT Manager (*Responsable Informatique*) is using this repository. In case of *an emergency where the IT Manager is unable to respond*, AGEPoly's Presidency and Administrator are able to use the encrypted archive and get access to every single component of AGEPoly's infrastructure.

The GnuPG software is used with symmetric encryption (AES-256 ) 
To use the scripts you need to have the following packages installed on a Linux machine: GnuPG and zip/unzip. Both are usually already installed.

## How to decrypt the archive - using the script (Linux)

- Clone (`git clone https://gitlab.com/agepoly/it/secrets.git`) or download this repository
- Execute the Bash script `./decrypt.sh`, the passphrase will be prompted if not already stored by the OS

## How to decrypt the archive - manually

[Install a GnuPG frontend](https://gnupg.org/software/frontends.html) and open the secret.zip.gpg file and enter the passphrase. Extract the secret.zip like any other ZIP.

## How to update the archive

- Clone (`git clone git@gitlab.com:agepoly/it/secrets.git`) this repository
- Execute the Bash script `./update.sh` (in a terminal to see the output)
- IMPORTANT: VERIFY THE PASSPHRASE AS THE PREVIOUS ONE WILL BE REPLACED
- Verify that all secrets are ignored by Git (`git add . && git status`) and push

# Public information

## How to generate a random passphrase from the EFF Large Wordlist ?

On Linux using "shuf" to pick 6 words at random (~77 bits of entropy)
```bash
shuf -n 6 --random-source=/dev/urandom public/eff_large_wordlist.txt
```

(NB: local list from: https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt)