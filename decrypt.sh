#!/usr/bin/env bash

# Checks

if [ ! -f "secret.zip.gpg" ]; then
    echo "secret.zip.gpg does not exist. Execute the script in the root folder of agepinfo-secrets"
    exit 1
fi

if ! unzip -v > /dev/null; then
    echo "unzip command unavailable, make sure it is installed"
    exit 2
fi

if ! gpg --version > /dev/null; then
    echo "zip command unavailable, make sure it is installed"
    exit 2
fi

if [ -d "secret" ]; then
    echo "secret folder already exists, delete it if you want to re-run decryption"
    exit 1
fi

# Decryption

echo "Performing decryption.."
if gpg -o secret.zip -d secret.zip.gpg; then
    echo "Decryption successful, unzipping output.."
    unzip secret.zip
    rm secret.zip
else 
    echo "Decryption failed, make sure the passphrase is correct"
    exit 3
fi
