#!/usr/bin/env bash

# Checks

if [ ! -f "secret.zip.gpg" ]; then
    echo "secret.zip.gpg does not exist. Execute the script in the root folder of agepinfo-secrets"
    exit 1
fi

if ! zip -v > /dev/null; then
    echo "zip command unavailable, make sure it is installed"
    exit 2
fi

if ! gpg --version > /dev/null; then
    echo "zip command unavailable, make sure it is installed"
    exit 2
fi

# Encryption

zip -r secret.zip secret

if [ -f "secret.zip.gpg" ]; then
    echo "deleting secret.zip.gpg.."
    rm secret.zip.gpg
fi

if gpg --batch --yes -c --cipher-algo AES256 secret.zip; then
    rm secret.zip
    echo "secret.zip.gpg updated !"
else
    echo "Encryption failed, secret.zip not deleted"
fi
